package com;

public class ForEachLoopDemo {
public static void main(String[] args) {
	int a[]= {10,20,30};
	for(int i:a) {
		System.out.println(i);
	}
	System.out.println("--------------------------------");
	double[] Per= {1.0,2.0,5.5,4.2};
	for(double i:Per) {
		System.out.println(i);
	}
	String[] fru= {"vishwa","mango"};
	for(String i:fru) {
		System.out.println(i);
}
}
}